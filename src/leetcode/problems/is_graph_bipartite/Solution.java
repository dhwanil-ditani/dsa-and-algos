package leetcode.problems.is_graph_bipartite;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	
	/**
	 * A graph is a bipartite graph if it is 2-colorable or not.
	 * @param graph
	 * @return whether the graph is Bipartite graph or not
	 */
	public boolean isBipartite(int[][] graph) {
		int[] color = new int[graph.length]; // color of all the vertices
		Arrays.fill(color, -1); // assign default color as no color
		Queue<Integer> q = new LinkedList<Integer>(); // queue for bfs
		for (int i=0; i<graph.length; i++) { // so that disconnected nodes does not get left out
			if (color[i] == -1) {
				color[i] = 0; // assign red color if no color is assigned
				q.add(i); // add to queue for bfs
			}
			while (!q.isEmpty()) {
				int v = q.poll();
				for (int j=0; j<graph[v].length; j++) { // assign all the neighbors of v with color different than that of v
					if (color[graph[v][j]] == color[v]) { // if a neighbor of v is already assigned a color which is same as v than the graph cannot be bipartite
						return false;
					}
					if (color[graph[v][j]] == -1) {
						color[graph[v][j]] = 1 - color[v];
						q.add(graph[v][j]);
					}
				}
			}
		}
		return true;
	}
}
