package leetcode.problems.is_graph_bipartite;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestCases {
	
	Solution solve;

	@BeforeEach
	void setUp() throws Exception {
		solve = new Solution();
	}

	@Test
	void Example1() {
		int[][] graph = {{1,2,3}, {0,2}, {0,1,3}, {0,2}};
		assertFalse(solve.isBipartite(graph));
	}
	
	@Test
	void Example2() {
		int[][] graph = {{1,3}, {0,2}, {1,3}, {0,2}};
		assertTrue(solve.isBipartite(graph));
	}

}
